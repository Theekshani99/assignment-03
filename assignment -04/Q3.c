#include <stdio.h>
int main() {
    int a, x = 0, y;
    printf("Enter an integer: ");
    scanf("%d", &a);
    while (a != 0)
        {
        y = a % 10;
        x = x * 10 + y;
        a = a/10;
       }
    printf("Reversed number = %d", x);
    return 0;
}
