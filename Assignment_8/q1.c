#include <stdio.h>
struct student {
    char firstName[20];
    char subject[20];
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter information about students:\n");


    for (i = 0; i < 5; ++i) {

        printf("Enter first name: ");
        scanf("%s", s[i].firstName);
        printf("Enter the subject: ");
        scanf("%s", s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("Displaying details about students:\n\n");


    for (i = 0; i < 5; ++i) {

       printf("First name: ");
        puts(s[i].firstName);
        printf("subject : ");
        puts(s[i].subject);
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
